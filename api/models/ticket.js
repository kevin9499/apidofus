let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let TicketSchema = new Schema({
    heure: {

        type: Number,

    },
    minute: {

        type: Number,

    },
    created: {

        type: Date, default: Date.now()
    }
});

module.exports = mongoose.model('Ticket', TicketSchema);