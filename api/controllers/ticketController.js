'use strict';

let mongoose = require('mongoose'),
    Ticket = require('../models/ticket');

exports.list_all_tickets = function(req, res) {
    Ticket.find({}, function(err, ticket) {
        if (err) {
            res.send(err);
        }else {
            let date = new Date()
            let newticket = []
            ticket.map( m => {
                if (m.created.getDate() === date.getDate() && m.created.getMonth() === date.getMonth()){
                    if( m.created.getHours()+1 === date.getHours()+1 && m.created.getMinutes() >= date.getMinutes()-1 && m.created.getMinutes() <= date.getMinutes()+1) {
                        m.heure = m.created.getHours() + 1
                        m.minute = m.created.getMinutes()
                        newticket.push(m)
                    }
                }
            })
            res.json(newticket)
        }
    });
};
exports.create_a_ticket = function(req, res) {
    let new_ticket = new Ticket(req.body);
    new_ticket.created = new Date()

    new_ticket.save(function(err, ticket) {
        if (err)
            res.send(err);
        else{
            //res.locals.io.emit("message",  {action: "created", type:"post", data: ticket});
            res.json(ticket);
        }

    });
};
