'use strict';
module.exports = function(app) {
    let ticketController = require('../controllers/ticketController');
    app.route('/ticket').
    get(ticketController.list_all_tickets).
    post(ticketController.create_a_ticket);
};
