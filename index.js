const express = require('express')
const app = express()
const axios = require('axios');
const http = require('http').createServer(app)
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mongo:27017/dofus',{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false, parameterLimit: 50000 }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());

let ticket_routes = require('./api/routes/ticketRoutes'); //importing route
ticket_routes(app); //register the route


http.listen(3000);
console.log('API is listening on port: ' + 3000);
